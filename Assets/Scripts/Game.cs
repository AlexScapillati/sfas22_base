
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Game : MonoBehaviour
{
    private Board _board;
    private Snake pSnake;

    [SerializeField] GameObject localPlayerPrefab;
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] UI _ui;
    [SerializeField] int NumberEnemies;
    [SerializeField] private Tutorial _tutorial;

    private double _gameStartTime;
    public bool _gameInProgress { private set; get; }
    
    public LocalPlayer player { private set; get; }
    public List<Enemy> enemies { get; private set; }

    public bool pause = false;

    private void SpawnEnemyInRandomBox()
    {
        var spawnPos = _board.FindClearRandomPositionInGrid();
        Vector3 rotation = Vector3.forward * Random.value;

        var enemy = Instantiate(enemyPrefab, transform).GetComponent<Enemy>();
        var snake = enemy.GetComponentInChildren<Snake>();
        Debug.Assert(enemy && snake);
        snake.transform.position = spawnPos;
        snake.transform.Rotate(rotation);
        enemies.Add(enemy);
    }

    private void Awake()
    {
        _board = transform.parent.GetComponentInChildren<Board>();
        _gameInProgress = false;
        var seed = DateTime.Now.Ticks.GetHashCode();
        Random.InitState(seed);
    }

    private void Start()
    {
        if (_board != null)
        {
            _board.Setup(BoardEvent);
        }

        if (_ui != null)
        {
            _ui.ShowMenu();
        }
    }

    public void OnClickedNewGame()
    {
        if (_board != null)
        {
            if (_tutorial)
                _tutorial.gameObject.SetActive(!_tutorial.bCompleted);

            _board.RechargeBoxes();

            var spawnPos = _board.FindClearRandomPositionInGrid();

            player = Instantiate(localPlayerPrefab, transform).GetComponent<LocalPlayer>();
            pSnake = player.GetComponentInChildren<Snake>();
            pSnake.transform.position = spawnPos;

            enemies = new List<Enemy>(NumberEnemies);

            for (int i = 0; i < NumberEnemies; i++)
            {
                SpawnEnemyInRandomBox();
            }
        }

        if (_ui != null)
        {
            _ui.HideMenu();
            _ui.ShowGame();
            _ui.HideResult();
        }
    }

    public void OnClickedExit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }

    public void OnClickedReset()
    {
        if (_board != null)
        {
            _board.Clear();
        }

        if (_ui != null)
        {
            _ui.HideResult();
            _ui.ShowMenu();
        }

        foreach (var enemy in enemies)
        {
            if (enemy)
                enemy.Destroy();
        }

        if (_tutorial)
        {
            if (!_tutorial.bCompleted)
            {
                _tutorial.Reset();
            }
            else
            {
                Destroy(_tutorial);
            }
        }
    }


    private void Update()
    {
        if (_ui != null)
        {
            _ui.UpdateTimer(_gameInProgress ? Time.realtimeSinceStartupAsDouble - _gameStartTime : 0.0);
            if (_gameInProgress) _ui.UpdateScore();

        }

        if (_gameInProgress && !pause)
        {

            int totalPoints = pSnake.Score;

            // every time an enemy dies

            foreach (var e in enemies)
            {
                if (e == null)
                {
                    enemies.Remove(e);
                    break;
                }
                else
                {
                    if (e.Snake)
                        totalPoints += e.Snake.Score;
                }
            }


            // player won
            if (enemies.Count == 0)
            {
                BoardEvent(Board.Event.Win);
            }


            // check if the player has revealed all the bombs so there is no more points to get
            if (pSnake)
            {
                if (totalPoints >= _board.NumberPointsToWin)
                    BoardEvent(Board.Event.Win);
            }
            else
            {
                BoardEvent(Board.Event.ClickedDanger);
            }

            if (_tutorial)
            {
                _tutorial.gameObject.SetActive(!_tutorial.bCompleted && _gameInProgress);
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape) && _gameInProgress)
        {
            TogglePause();
        }

        if (enemies != null)
            foreach (var e in enemies)
            {
                if (e)
                    e.enabled = !pause;
            }

        if (player)
            player.enabled = !pause;
    }

    public void TogglePause()
    {
        pause = !pause;
        if (_ui != null) _ui.TogglePause();
    }

    private void BoardEvent(Board.Event eventType)
    {
        if(pause) return;

        if (!_gameInProgress)
        {
            _gameInProgress = true;
            _gameStartTime = Time.realtimeSinceStartupAsDouble;
        }

        if (eventType == Board.Event.ClickedDanger && _ui != null)
        {
            _ui.HideGame();
            _ui.ShowResult(success: false);
            if (player) player.Destroy();
            _gameInProgress = false;
        }

        if (eventType == Board.Event.Win && _ui != null)
        {
            _ui.HideGame();
            _ui.ShowResult(success: true);
            if (player) player.Destroy();
            _gameInProgress = false;
        }
    }


}
