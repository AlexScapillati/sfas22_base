using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreTable : MonoBehaviour
{
    [SerializeField] private Game _game;
    [SerializeField] private Transform _highscoreEntryPrefab;
    [SerializeField] private Transform _entryContainter;
    [SerializeField] private float entryHeight;
    [SerializeField] private int _maxEntriesShowing;

    private List<Entry> entries;

    // update the list every 60 frames
    private const int maxUpdateFrames = 60;
    private int frameTimer = maxUpdateFrames;


    // Start is called before the first frame update
    void LateStart()
    {
        Debug.Assert(_game);
        Debug.Assert(_highscoreEntryPrefab);
        Debug.Assert(_entryContainter);

        _highscoreEntryPrefab.gameObject.SetActive(false);

        Init();
    }

    private void TableUpdate()
    {
        if (entries == null)
        {
            Init();
        }
        else
        {
            // garbage eliminator
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].Snake) continue;

                Destroy(entries[i].rt.gameObject);
                entries.Remove(entries[i]);
            }
            
            // sort the entity list by score
            entries.Sort((e, w) => w.Iscore.CompareTo(e.Iscore));

            // this will recalculate the entry position and its place
            for (int i = 0; i < entries.Count; i++)
            {
                entries[i].rt.anchoredPosition = new Vector2(0, -entryHeight * i);
                entries[i].Place = GetRankFromScore(i + 1);
            }

            // update all the entries
            foreach (var entry in entries)
            {
                entry.Update();
            }

            // show only the first entries
            for (int i = 0; i < entries.Count; i++)
            {
                entries[i].rt.gameObject.SetActive(entries[i].Snake && i <= _maxEntriesShowing);
            }
        }
    }

    void Init()
    {
        var enemies = _game.enemies;

        if (enemies == null) return;

        List<Snake> snakes = new List<Snake>(enemies.Count + 1);

        foreach (var e in enemies)
        {
            snakes.Add(e.Snake);
        }

        var p = GameObject.FindGameObjectWithTag("Player");
        snakes.Add(p.GetComponentInChildren<Snake>());

        entries = new List<Entry>(snakes.Count);

        for (int i = 0; i < snakes.Count; i++)
        {
            Transform entryTransform = Instantiate(_highscoreEntryPrefab, _entryContainter) as RectTransform;
            RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
            entryRectTransform.anchoredPosition = new Vector2(0, -entryHeight * i);
            entryTransform.gameObject.SetActive(true);

            int rank = i + 1;
            string rankString = GetRankFromScore(rank);

            Entry e = new Entry();

            e.Snake = snakes[i];
            e.rt = entryRectTransform;

            var place = entryTransform.Find("Place");
            if (place)
            {
                var txt = place.GetComponent<TextMeshProUGUI>();
                if (txt)
                {
                    txt.text = rankString;
                    e.Place = rankString;
                }
            }

            var name = entryTransform.Find("Name");
            if (name)
            {
                var txt = name.GetComponent<TextMeshProUGUI>();
                if (txt)
                {
                    if (snakes[i])
                        if (snakes[i].GetComponentInParent<Enemy>()) txt.text = "BOT" + i;
                        else txt.text = "YOU";
                    else snakes.Remove(snakes[i]);
                        
                    
                    e.Name = txt.text;
                }
            }

            var score = entryTransform.Find("Score");
            if (score)
            {
                var txt = score.GetComponent<TextMeshProUGUI>();
                if (txt)
                {
                    txt.text = snakes[i].Score.ToString();
                    e.Score = txt.text;
                    e.Iscore = snakes[i].Score;
                }
            }
            entries.Add(e);
        }
    }


    void LateUpdate()
    {
        if (!_game._gameInProgress)
        {
            if (entries == null || entries.Count == 0)
            {
                return;
            }

            foreach (var entry in entries)
            {
                Destroy(entry.rt.gameObject);
            }

            entries = null;
            return;
        }

        if (frameTimer < 0)
        {
            frameTimer = maxUpdateFrames;
        }
        else
        {
            frameTimer--;
        }

        TableUpdate();
    }


    public string GetPlayerPosition()
    {
        int s = _game.enemies.Count;

        if (_game.enemies.Count < 1) return "1ST";

        for (int i = _game.enemies.Count - 1; i >= 0; i--)
        {
            if (_game.enemies[i])
            {
                var snake = _game.enemies[i].GetComponent<Enemy>().Snake;
                if (snake)
                {
                    var score = snake.Score;
                    if (score < _game.player.Snake.Score)
                        s--;
                    else
                        break;
                }
            }
        }

        s++;
        return GetRankFromScore(s);
    }

    private string GetRankFromScore(int score)
    {
        string result = score switch
        {
            1 => "1ST",
            2 => "2ND",
            3 => "3RD",
            _ => score + "TH"
        };

        return result;
    }
}




public class Entry
{
    public String Name;
    public String Place;
    public String Score;
    public int Iscore;

    public Snake Snake;

    public RectTransform rt;

    private TextMeshProUGUI scoretx;
    private TextMeshProUGUI nametx;
    private TextMeshProUGUI placetx;

    public void Update()
    {
        Score = Snake.Score.ToString();
        Iscore = Snake.Score;

        if (!scoretx)
        {
            var score = rt.Find("Score");
            if (score)
            {
                scoretx = score.GetComponent<TextMeshProUGUI>();
            }
        }

        scoretx.text = Score;

        if (!nametx)
        {
            var name = rt.Find("Name");
            if (name)
            {
                nametx = name.GetComponent<TextMeshProUGUI>();
            }
        }
        nametx.text = Name;

        if (!placetx)
        {
            var place = rt.Find("Place");
            if (place)
            {
                placetx = place.GetComponent<TextMeshProUGUI>();
            }
        }
        placetx.text = Place;
    }
}