using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{

    [SerializeField] private List<Sound> sounds;
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private AudioMixerGroup musicGroup;
    [SerializeField] private AudioMixerGroup effectsGroup;
    
    // Start is called before the first frame update
    void Awake()
    {

        foreach (var sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
            sound.source.outputAudioMixerGroup = sound.isEffect ? effectsGroup : musicGroup;
        }
    }

    void Start()
    {
        Play("MainTheme");
    }

    public void Play(string name)
    {
        var sound = sounds.Find(s => s.name == name);
        sound?.source.Play();
    }

    public void SetMusicVolume(float value)
    {
        audioMixer.SetFloat("Music", Mathf.Log10(value) * 20);
    }

    public void SetEffectsVolume(float value)
    {
        audioMixer.SetFloat("Effects", Mathf.Log10(value) * 20);
    }

    public void SetMasterVolume(float value)
    {
        audioMixer.SetFloat("Master", Mathf.Log10(value) * 20);
    }

    public void ToggleMute()
    {
        foreach (var s in sounds)
        {
            s.source.mute = !s.source.mute;
        }
    }
}

[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;
    public bool isEffect;

    [Range(0f, 1f)] public float volume;
    public float pitch;
    public bool loop;

    [HideInInspector]
    public AudioSource source;
}