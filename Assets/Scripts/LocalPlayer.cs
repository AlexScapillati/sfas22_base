
using UnityEngine;
using UnityEngine.Assertions;

public class LocalPlayer : MonoBehaviour
{
    public Snake Snake { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        Snake = GetComponentInChildren<Snake>();
        Assert.IsNotNull(Snake);
    }

    // Update is called once per frame
    void Update()
    {
        var x = Input.GetAxisRaw("Horizontal");
        if (Snake)
        {
            Snake.Move(x);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Destroy()
    {
        if(Snake)
            Snake.Destroy();

        Destroy(gameObject);
    }
}
