using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeSegment : MonoBehaviour
{
    public SnakeSegment NextSegment { private set; get; }
    public float Speed { set; get; } 
    public Transform Parent { set; get; }
    public float Scale { set; get; }

    [SerializeField] private Sprite middleSprite;
    [SerializeField] private Sprite tailSprite;

    private Game game;

    private SpriteRenderer sr;

   // Start is called before the first frame update
   void Start()
    {
        game = FindObjectOfType<Game>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (game.pause) return;
        
        if (Parent)
        {
            // Angle relative to the prev segment
            Vector2 v = transform.position - Parent.transform.position;
            float angle = Vector2.Dot(v, transform.right);
            
            // Calculate rotation depending on the angle calculated (right if the object is at the right, left if the object is at left)
            Vector3 deltaRotation = Vector3.forward * angle  * Mathf.PI * 100 ;

            // Move forward
            transform.position += transform.up * Speed * Time.deltaTime;

            // Rotate this segment to face the previous
            transform.Rotate(deltaRotation * Time.deltaTime* Speed * 5f);
        }
        else
        {
            Destroy(gameObject, 0.01f);
        }

        if (sr != null) sr.sprite = NextSegment != null ? middleSprite : tailSprite;
        else sr = GetComponent<SpriteRenderer>();
        
    }

    public void AddSegment()
    {

        if (NextSegment)
        {
            NextSegment.AddSegment();
        }
        else
        {
            if (sr == null) sr = GetComponent<SpriteRenderer>();
            Vector3 spawnPos = transform.position + transform.up * -sr.sprite.bounds.size.sqrMagnitude / 10f;
            spawnPos.z += 0.01f;
            NextSegment = Instantiate(this, spawnPos, transform.rotation);
            NextSegment.Scale = Scale;
            NextSegment.Speed = Speed;
            NextSegment.Parent = transform;
        }

    }

    public Snake GetSnake()
    {
        var tmp = Parent;
        do
        {
            var snakeSegment = tmp.GetComponent<SnakeSegment>();
            if(snakeSegment) tmp = snakeSegment.Parent;

        } while (tmp.GetComponent<Snake>() == null);

        return tmp.GetComponent<Snake>();
    }
    
}
