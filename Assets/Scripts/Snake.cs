
using UnityEngine;

public class Snake : MonoBehaviour
{
    [SerializeField] SnakeSegment SegmentPrefab;

    private SnakeSegment NextSegment = null;
    private float Scale;
    private float Speed = 3f;

    private BoxCollider2D boxCollider;
    private SpriteRenderer SpriteRenderer;
    private RectTransform grid;

    public int Score { set; get; }

    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();

        AddSegment();

        Scale = transform.localScale.magnitude;
        Score = 0;

        var board = FindObjectOfType<Board>();
        if (board)
        {
            grid = board._rect;
        }
        else
        {
            Debug.Log("Board not found");
        }
    }

    void Update()
    {
    }

    public void Move(float horizontal)
    {
        var offset = Mathf.Sin(Time.realtimeSinceStartup * 10) / 10;

        transform.Rotate(Vector3.forward, -horizontal * Speed / Mathf.PI + offset);

        transform.position += transform.up * Speed * Time.deltaTime;

        Debug.DrawRay(transform.position, transform.up, Color.black, 0.1f);

        if (grid)
        {
            Vector2 pos = transform.localPosition;
            Vector2 prevPos = transform.position;

            // if the snake is not in the grid

            bool check = false;

            var xMin = grid.rect.xMin * grid.localScale.x;
            var xMax = grid.rect.xMax * grid.localScale.x;
            var yMin = grid.rect.yMin * grid.localScale.y;
            var yMax = grid.rect.yMax * grid.localScale.y;

            // teleport it in the other side
            if (pos.x < xMin)
            {
                pos.x = xMax;
                check = true;
            }
            else if (pos.x > xMax)
            {
                pos.x = xMin;
                check = true;
            }
            else if (pos.y < yMin)
            {
                pos.y = yMax;
                check = true;
            }
            else if (pos.y > yMax)
            {
                pos.y = yMin;
                check = true;
            }

            if (check)
            {
                // move the snake head
                transform.localPosition = pos;

                Vector2 newPos = transform.position;

                // Get the offset 
                var off = newPos - prevPos;

                // teleport all the segments as well
                var segment = NextSegment;
                while (segment)
                {
                    segment.transform.position += new Vector3(off.x, off.y, 0);
                    segment = segment.NextSegment;
                }
            }
        }
    }


    public void AddSegment()
    {

        if (transform.parent.GetComponent<LocalPlayer>())
            FindObjectOfType<AudioManager>().Play("BoxReveal");

        Score++;

        if (NextSegment != null)
        {
            NextSegment.AddSegment();
        }
        else
        {
            Vector3 spawnPos = transform.position + transform.up * -SpriteRenderer.sprite.bounds.size.sqrMagnitude / 7.5f;
            NextSegment = Instantiate(SegmentPrefab, spawnPos, transform.rotation);

            NextSegment.Scale = Scale;
            NextSegment.Speed = Speed;
            NextSegment.Parent = transform;
        }
    }

    public void AddSegments(int count)
    {
        for (int i = 0; i < count; i++)
        {
            AddSegment();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(!other.gameObject.activeSelf || !boxCollider) return;


        // On a snake there are two collider, both triggers
        // The box collider is the actual shape of the snake
        // the polygon collider is used for the snake's vision
        // unity does not allow to distinguish to witch collider the collision happened
        // so i am going to use a bit of imagination
        // check the distance between the other collider and the box collider

        var collDistance = other.Distance(boxCollider);

        //if the other one is a polygon collider, don't perform collision
        if (other as PolygonCollider2D) return;

        // if the other collider and the box collider are overlapping
        if (collDistance.isOverlapped)
        {
            // if overlapped with a box
            Box box = other.GetComponent<Box>();
            if (box != null)
            {
                if (box.IsDangerous)
                {
                    Destroy();
                }

                // if the box is not already revealed
                if (!box.Clicked)
                {
                    // add the box number as segments
                    // this will be 0 if the box is empty
                    AddSegments(box.DangerNearby);
                }

                // reveal the box
                box.OnClick();

                return;
            }

            // if overlapped with another snake
            Snake snake = other.GetComponentInChildren<Snake>();
            if (snake)
            {
                // check witch is the stronger
                // depending on the size of the snake, add the segments to the other snake 
                if (snake.Score > Score)
                {
                    snake.AddSegments(Score);
                    Destroy();
                }
                else
                {
                    AddSegments(snake.Score);
                    snake.Destroy();
                }

                return;
            }

            // if colliding with a segment 
            SnakeSegment segment = other.GetComponent<SnakeSegment>();
            if (segment)
            {
                // if the segment is NOT part of this snake 
                if (segment.GetSnake() != this)
                    // Destroy this snake 
                    Destroy();
            }
        }
        else
        {
            // Check if this snake is an enemy

            Enemy enemy = GetComponentInParent<Enemy>();
            if (enemy)
            {
                // if so make it handle collision in the right script
                enemy.MyOnTriggerEnter(other);
            }
        }
    }

    public void Destroy()
    {
        if (transform.parent.GetComponent<LocalPlayer>())
            FindObjectOfType<AudioManager>().Play("SnakeDead");


        if (NextSegment)
            Destroy(NextSegment.gameObject);

        Destroy(transform.parent.gameObject);
        Destroy(gameObject);
    }
}

