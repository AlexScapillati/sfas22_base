using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    public Snake Snake { get; private set; }

    private Snake TargetSnake = null;
    private Transform ThingToAvoid = null;
    private float RandomSeed;

    [SerializeField] private float RandomDirection;

    // Start is called before the first frame update
    void Start()
    {
        Snake = GetComponentInChildren<Snake>();
        RandomSeed = Random.Range(0f,100f);
    }

    // Update is called once per frame
    void Update()
    {
        // Enemy AI
        if (!Snake) Destroy();

        // Wonder about
        RandomDirection = Mathf.PerlinNoise(Time.realtimeSinceStartup + RandomSeed, 0);
        RandomDirection = Mathf.SmoothStep(-1f,1f,RandomDirection);

        // Avoid bombs and segments
        if (ThingToAvoid)
        {

            Vector2 bombPos = ThingToAvoid.position;
            Vector2 snakePos = Snake.transform.position;

            // vector between this snake and the near bomb
            Vector3 SnakeVBomb = bombPos - snakePos;

            float distance = SnakeVBomb.sqrMagnitude;

            // check if the bomb is in front or behind the snake
            var SdotV = Vector3.Dot(SnakeVBomb.normalized, Snake.transform.up);

            Debug.DrawLine(snakePos, bombPos, Color.red);

            var leftOrRight = Mathf.Sign(Vector3.Dot(SnakeVBomb, Snake.transform.up));

            if (SdotV > 0.3f) //coefficient)
            {
                // if the bomb is in front we start to deviate the snake trajectory
                RandomDirection = -leftOrRight * (1f / distance) * 10f;
            }
            else
            {
                // if the bomb is not our interest, not worry about it
                ThingToAvoid = null;
            }
        }

        // if some snake is near
        if (TargetSnake)
        {
            // try to kill it
            Vector3 SnakeVSnake = Snake.transform.position - TargetSnake.transform.position;
            float distance = SnakeVSnake.sqrMagnitude;

            var aimDir = Snake.transform.position - (TargetSnake.transform.position + TargetSnake.transform.up);
            
            Debug.DrawRay(Snake.transform.position,aimDir,Color.blue);

            var SdotV = Vector3.Dot(SnakeVSnake.normalized, Snake.transform.right);
            var leftOrRight = Mathf.Sign(Vector3.Dot(SnakeVSnake, Snake.transform.up));

            if (SdotV > 0.3)
            {
                RandomDirection += -leftOrRight * (1f/distance);
            }

            // if the snake is not our interest, not worry about it
            if (distance > 100f)
            {
                TargetSnake = null;
            }
        }

        // perform movement
        Snake.Move(RandomDirection);
    }

    public void Destroy()
    {
        if (Snake)
            Snake.Destroy();

        if (gameObject)
            Destroy(gameObject);
    }


    public void MyOnTriggerEnter(Collider2D other)
    {
        // See code in snake.cs

        // if collided with a snake
        var snake = other.GetComponent<Snake>();
        if (snake)
        {
            // check if it is not this one
            if (snake != Snake)
            {
                TargetSnake = snake;
            }
        }


        var box = other.GetComponent<Box>();
        if (box)
        {
            if (box.IsDangerous)
            {
                if (ThingToAvoid)
                {
                    if (Distance(ThingToAvoid.gameObject) < Distance(box.gameObject))
                    {
                        return;
                    }
                }
                
                ThingToAvoid = box.transform;
            }
        }

        var segment = other.GetComponent<SnakeSegment>();
        if (segment)
        {
            if (Snake != segment.GetSnake())
            {
                if (ThingToAvoid)
                {

                    if (Distance(ThingToAvoid.gameObject) < Distance(segment.gameObject))
                    {
                        return;
                    }
                }
                
                ThingToAvoid = segment.transform;
            }
        }

    }

    // Helper funtion for calculating the distance between this game object and the given game object
    public float Distance(GameObject o)
    {
        Vector2 oPos = o.transform.position;
        Vector2 snakePos = Snake.transform.position;
        return (oPos - snakePos).sqrMagnitude;
    }
}
