using System;
using UnityEngine;
using TMPro;
using System.Collections;

public class UI : MonoBehaviour
{
    [SerializeField] private CanvasGroup Menu;
    [SerializeField] private CanvasGroup Game;
    [SerializeField] private CanvasGroup Result;
    [SerializeField] private TMP_Text TimerText;
    [SerializeField] private TMP_Text ResultText;
    [SerializeField] private TMP_Text ResultScore;
    [SerializeField] public ScoreTable ScoreTable;
    [SerializeField] private CanvasGroup Settings;
    [SerializeField] private CanvasGroup Pause;

    private static readonly string[] ResultTexts = { "Game Over!", "You Win!!" };
    private static readonly string ScoreTextt = "You: ";
    private static readonly float AnimationTime = 0.5f;

    public void ShowMenu()
    {
        StartCoroutine(ShowCanvas(Menu, 1.0f));
    }

    public void ShowGame()
    {
        StartCoroutine(ShowCanvas(Game, 1.0f));
    }

    public void ShowResult(bool success)
    {
        if (ResultText != null)
        {
            ResultText.text = ResultTexts[success ? 1 : 0];

            if (ResultScore)
            {
                if (success)
                {
                    ResultScore.text = "";
                }
            }
        }


        StartCoroutine(ShowCanvas(Result, 1.0f));
    }

    public void Back()
    {
        if (Pause.alpha > .1f)
        {
            StartCoroutine(ShowCanvas(Pause, .0f));
        }
        else if (Settings.alpha > .1f)
        {
            StartCoroutine(ShowCanvas(Settings, .0f));

            var game = FindObjectOfType<Game>();
            if (game)
            {
                if (game._gameInProgress)
                {
                    TogglePause();
                }
                else
                {
                    ShowMenu();
                }
            }
        }
    }

    public void ToggleSettings()
    {
        StartCoroutine(Settings.alpha >= .1f ? ShowCanvas(Settings, .0f) : ShowCanvas(Settings, 1.0f));
    }

    public void TogglePause()
    {
        StartCoroutine(Pause.alpha >= .1f ? ShowCanvas(Pause, .0f) : ShowCanvas(Pause, 1.0f));
    }

    public void HideMenu()
    {
        StartCoroutine(ShowCanvas(Menu, 0.0f));
    }

    public void HideGame()
    {
        StartCoroutine(ShowCanvas(Game, 0.0f));
    }

    public void HideResult()
    {
        StartCoroutine(ShowCanvas(Result, 0.0f));
    }


    public void UpdateTimer(double gameTime)
    {
        if (TimerText != null)
        {
            TimerText.text = FormatTime(gameTime);
        }

    }

    public void UpdateScore()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        if (player)
        {
            var playerScore = player.GetComponentInChildren<Snake>().Score;

            if (ResultScore)
            {
                ResultScore.text = ScoreTextt + playerScore + " , Position: ";
                if (ScoreTable)
                {
                    var pos = ScoreTable.GetPlayerPosition();
                    ResultScore.text += pos;
                }
            }

        }
    }

    private void Awake()
    {
        if (Menu != null)
        {
            Menu.gameObject.SetActive(true);
            Menu.alpha = 0.0f;
            Menu.interactable = false;
            Menu.blocksRaycasts = false;
        }

        if (Game != null)
        {
            Game.gameObject.SetActive(true);
            Game.alpha = 0.0f;
            Game.interactable = false;
            Game.blocksRaycasts = false;
        }

        if (Result != null)
        {
            Result.gameObject.SetActive(true);
            Result.alpha = 0.0f;
            Result.interactable = false;
            Result.blocksRaycasts = false;
        }


        if (Pause != null)
        {
            Pause.gameObject.SetActive(true);
            Pause.alpha = 0.0f;
            Pause.interactable = false;
            Pause.blocksRaycasts = false;
        }


        if (Settings != null)
        {
            Settings.gameObject.SetActive(true);
            Settings.alpha = 0.0f;
            Settings.interactable = false;
            Settings.blocksRaycasts = false;
        }
    }

    private static string FormatTime(double seconds)
    {
        float m = Mathf.Floor((int)seconds / 60);
        float s = (float)seconds - (m * 60);
        string mStr = m.ToString("00");
        string sStr = s.ToString("00.000");
        return string.Format("{0}:{1}", mStr, sStr);
    }

    private IEnumerator ShowCanvas(CanvasGroup group, float target)
    {
        if (group != null)
        {
            float startAlpha = group.alpha;
            float t = 0.0f;

            group.interactable = target >= 1.0f;
            group.blocksRaycasts = target >= 1.0f;

            while (t < AnimationTime)
            {
                t = Mathf.Clamp(t + Time.deltaTime, 0.0f, AnimationTime);
                group.alpha = Mathf.SmoothStep(startAlpha, target, t / AnimationTime);
                yield return null;
            }
        }
    }
}
