using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    private GameObject Player;

    [SerializeField] float Smoothness;
    [SerializeField] Vector3 offset;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Player == null)
        {
            var snakes = GameObject.FindGameObjectsWithTag("Player");

            foreach (var snake in snakes)
            {
                if (snake.GetComponent<LocalPlayer>().GetComponentInChildren<Snake>())
                {
                    Player = snake.GetComponent<LocalPlayer>().GetComponentInChildren<Snake>().gameObject;
                    return;
                }
            }

        }
        else
        {
            var newPosition = Player.transform.position + offset;
            newPosition = Vector3.Lerp(transform.position, newPosition, Smoothness);
            transform.position = newPosition;
        }
    }
}
