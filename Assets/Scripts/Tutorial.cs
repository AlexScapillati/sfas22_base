using UnityEngine;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private GameObject[] popUps;
    private int currentPopup;

    private const float startTimer = 3f; // three second timer
    private float timer = startTimer * 2; // actual timer

    public bool bCompleted { get; private set; } = false;

    private int show = 1;

    private Game game;


    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
        game = FindObjectOfType<Game>();
    }

    // Update is called once per frame
    void Update()
    {
        if (game && game.pause) return;

        if (bCompleted) return;

        if (Input.GetKeyDown(KeyCode.T))
        {
            bCompleted = true;
            Time.timeScale = 1;
            gameObject.SetActive(false);
            return;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentPopup++;
        }

        if (timer < 0)
        {
            timer = startTimer;
            show++;
            if (show % 2 == 0) currentPopup++;
        }
        else
        {
            timer -= Time.deltaTime;
        }


        var isEven = show % 2 == 0;

        Time.timeScale = isEven ? 0.4f : 1f;

        for (int i = 0; i < popUps.Length; i++)
        {
            popUps[i].SetActive(currentPopup == i-2 && isEven);

            if (currentPopup != popUps.Length - 1) continue;

            Time.timeScale = 1;
            bCompleted = true;
            gameObject.SetActive(false);
            break;
        }
    }

    public void Reset()
    {
        timer = startTimer;
        show = 1;
        currentPopup = 0;
        Time.timeScale = 1f;
    }

    private void OnDestroy()
    {
        Reset();
    }
}
