using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = Unity.Mathematics.Random;

public class Board : MonoBehaviour
{
    public enum Event
    {
        ClickedBlank,
        ClickedNearDanger,
        ClickedDanger,
        Win
    };

    [SerializeField] private Box BoxPrefab;

    [SerializeField] private int Width = 10;
    [SerializeField] private int Height = 10;
    [SerializeField] private int NumberOfDangerousBoxes = 10;

    [SerializeField] public int NumberPointsToWin { get; private set; }

    public Box[][] _grid { get; private set; }
    private Vector2Int[] _neighbours;
    public RectTransform _rect { get; private set; }
    private Action<Event> _clickEvent;

    private int NumberBombsClicked;

    public void Setup(Action<Event> onClickEvent)
    {
        _clickEvent = onClickEvent;
        Clear();
    }

    public void Clear()
    {
        for (int row = 0; row < Height; ++row)
        {
            for (int column = 0; column < Width; ++column)
            {
                _grid[row][column].StandDown();
            }
        }

        NumberBombsClicked = 0;
    }
    
    public void RechargeBoxes()
    {
        int numberOfItems = Width * Height;
        List<bool> dangerList = new List<bool>(numberOfItems);

        for (int count = 0; count < numberOfItems; ++count)
        {
            dangerList.Add(count < NumberOfDangerousBoxes);
        }

        dangerList.RandomShuffle();

        for (int row = 0; row < Height; ++row)
        {
            for (int column = 0; column < Width; ++column)
            {
                int index = row * Width + column;
                var dangerNearBy = CountDangerNearby(dangerList, index);
                _grid[row][column].Charge(dangerNearBy, dangerList[index], OnClickedBox);

                NumberPointsToWin += dangerNearBy;
            }
        }
    }

    private void Awake()
    {
        GenerateBoard();
    }

    private void GenerateBoard()
    {
        _grid = null;
        _grid = new Box[Width][];
        _rect = transform as RectTransform;
        RectTransform boxRect = BoxPrefab.transform as RectTransform;

        _rect.sizeDelta = new Vector2(boxRect.sizeDelta.x * Width, boxRect.sizeDelta.y * Height);
        Vector2 startPosition = _rect.anchoredPosition - (_rect.sizeDelta * 0.5f) + (boxRect.sizeDelta * 0.5f);
        startPosition.y *= -1.0f;

        _neighbours = new Vector2Int[8]
        {
            new Vector2Int(-Width - 1, -1),
            new Vector2Int(-Width, -1),
            new Vector2Int(-Width + 1, -1),
            new Vector2Int(-1, 0),
            new Vector2Int(1, 0),
            new Vector2Int(Width - 1, 1),
            new Vector2Int(Width, 1 ),
            new Vector2Int(Width + 1, 1)
        };

        for (int row = 0; row < Width; ++row)
        {
            _grid[row] = new Box[Height];

            GameObject rowObj = new GameObject(string.Format("Row{0}", row), typeof(RectTransform));
            RectTransform rowRect = rowObj.transform as RectTransform;
            rowRect.SetParent(transform);
            rowRect.anchoredPosition = new Vector2(_rect.anchoredPosition.x, startPosition.y - (boxRect.sizeDelta.y * row));
            rowRect.sizeDelta = new Vector2(boxRect.sizeDelta.x * Width, boxRect.sizeDelta.y);
            rowRect.localScale = Vector2.one;
            rowObj.transform.position.Set(startPosition.x,startPosition.y,0);

            for (int column = 0; column < Height; ++column)
            {
                int index = row * Width + column;
                _grid[row][column] = Instantiate(BoxPrefab, rowObj.transform);
                _grid[row][column].Setup(index, row, column);
                RectTransform gridBoxTransform = _grid[row][column].transform as RectTransform;
                _grid[row][column].name = string.Format("ID{0}, Row{1}, Column{2}", index, row, column);
                gridBoxTransform.anchoredPosition = new Vector2(startPosition.x + (boxRect.sizeDelta.x * column), 0.0f);
            }
        }
    }

    public Vector3 FindClearRandomPositionInGrid()
    {
        // Find a random position to spawn the snakes
        Vector3 spawnPos;
        bool isOccupied;

        do
        {
            int row = UnityEngine.Random.Range(0,Height);
            int column = UnityEngine.Random.Range(0, Width);

            var box = _grid[row][column];
            spawnPos = box.transform.position;
            isOccupied = box.IsDangerous || box.DangerNearby > 0;

            // check in front of the snake too
            if (!isOccupied)
            {
                var nextRow = (row - 1) % _grid.Length - 1;
                if (nextRow < 0) nextRow = _grid.Length + nextRow;
                var nextBox = _grid[nextRow][column];
                isOccupied = nextBox.IsDangerous || nextBox.DangerNearby > 0;
            }

        } while (isOccupied);

        return spawnPos;
    }


    private int CountDangerNearby(List<bool> danger, int index)
    {
        int result = 0;
        int boxRow = index / Width;

        if (!danger[index])
        {
            for (int count = 0; count < _neighbours.Length; ++count)
            {
                int neighbourIndex = index + _neighbours[count].x;
                int expectedRow = boxRow + _neighbours[count].y;
                int neighbourRow = neighbourIndex / Width;
                result += (expectedRow == neighbourRow && neighbourIndex >= 0 && neighbourIndex < danger.Count && danger[neighbourIndex]) ? 1 : 0;
            }
        }

        return result;
    }

    private void OnClickedBox(Box box)
    {
        Event clickEvent = Event.ClickedBlank;

        if (box.IsDangerous)
        {
            //clickEvent = Event.ClickedDanger;
            NumberBombsClicked++;
        }
        else if (box.DangerNearby > 0)
        {
            clickEvent = Event.ClickedNearDanger;
        }
        else
        {
            //ClearNearbyBlanks(box);
        }

        if(NumberBombsClicked >= NumberOfDangerousBoxes)
        {
            clickEvent = Event.ClickedDanger;
        }
        
        _clickEvent?.Invoke(clickEvent);
    }

    private bool CheckForWin()
    {
        bool Result = true;
        
        foreach (var row in _grid)
        {
            foreach (var box in row)
            {
                if (!box.IsDangerous && box.IsActive)
                {
                    return false;
                }
            }
        }

        return Result;
    }

    private void ClearNearbyBlanks(Box box)
    {
        RecursiveClearBlanks(box);
    }

    private void RecursiveClearBlanks(Box box)
    {
        if (!box.IsDangerous)
        {
            box.Reveal();

            if (box.DangerNearby == 0)
            {
                for (int count = 0; count < _neighbours.Length; ++count)
                {
                    int neighbourIndex = box.ID + _neighbours[count].x;
                    int expectedRow = box.RowIndex + _neighbours[count].y;
                    int neighbourRow = neighbourIndex / Width;
                    bool correctRow = expectedRow == neighbourRow;
                    neighbourIndex /= Height;
                    bool active = neighbourIndex >= 0 && neighbourIndex < Width * Height && _grid[neighbourRow][neighbourIndex].IsActive;

                    if (correctRow && active)
                    {
                        RecursiveClearBlanks(_grid[neighbourRow][neighbourIndex]);
                    }
                }
            }
        }
    }
}
